package auth

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
)

type Params struct {
	SecretKey []byte
}

type JwtResolver struct {
	params Params
}

func NewJwtResolver(params Params) JwtResolver {
	return JwtResolver{
		params: params,
	}
}

func (r JwtResolver) Create(claims jwt.Claims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(r.params.SecretKey)
}

func (r JwtResolver) Parse(tokenString string, claims jwt.Claims) error {
	parsedToken, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return r.params.SecretKey, nil
	})
	if err != nil {
		return errors.New("token is not valid")
	}
	if parsedToken == nil || !parsedToken.Valid {
		return errors.New("token is not valid")
	}

	return nil
}

func (r JwtResolver) generateToken(claims jwt.Claims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(r.params.SecretKey)
}
